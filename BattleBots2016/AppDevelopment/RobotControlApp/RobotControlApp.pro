#-------------------------------------------------
#
# Project created by QtCreator 2016-10-31T16:25:37
#
#-------------------------------------------------

QT       += qml quick bluetooth

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RobotControlApp
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    arduino101ble.cpp

HEADERS  += mainwindow.h \
    arduino101ble.h

#FORMS    += mainwindow.ui

CONFIG += c++11
#MOBILITY =

RESOURCES += \
    appresources.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

