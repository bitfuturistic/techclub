//#include "mainwindow.h"
#include <QtCore/QLoggingCategory>
#include <QQmlContext>
#include <QGuiApplication>
#include <QQuickView>
#include "arduino101ble.h"

int main(int argc, char *argv[])
{
    QGuiApplication a(argc, argv);
    QQuickView *view = new QQuickView;
    Arduino101 robotController;
    view->rootContext()->setContextProperty("robotController",&robotController);
    view->setSource(QUrl("qrc://assets/main.qml"));
    view->setResizeMode(QQuickView::SizeRootObjectToView);
    view->show();

    return a.exec();
}
