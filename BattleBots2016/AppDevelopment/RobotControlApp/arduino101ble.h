#ifndef ARDUINO101BLE_H
#define ARDUINO101BLE_H

#include <QObject>

#include <QString>
#include <QDebug>
#include <QDateTime>
#include <QVector>
#include <QTimer>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothDeviceInfo>
#include <QLowEnergyController>
#include <QLowEnergyService>

QT_USE_NAMESPACE
class Arduino101BLE : public QObject
{
    Q_OBJECT
public:
    Arduino101BLE();
    ~Arduino101BLE();

};

#endif // ARDUINO101BLE_H
