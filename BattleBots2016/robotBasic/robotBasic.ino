/*
 * Copyright (c) 2016 Intel Corporation.  All rights reserved.
 * See the bottom of this file for the license terms.
 */

#include <CurieBLE.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

Adafruit_MotorShield AFMS = Adafruit_MotorShield();

Adafruit_DCMotor *myMotorA = AFMS.getMotor(1);
Adafruit_DCMotor *myMotorB = AFMS.getMotor(2);
const int ledPin = 13; // set ledPin to on-board LED
const int actionPin = 6; // set buttonPin to digital pin 4

int healthPoints=100;
int prevHealthPoints=100;

BLEPeripheral blePeripheral; // create peripheral instance
BLEService ledService("19B10010-E8F2-537E-4F6C-D104768A1214"); // create service
BLEService dirService("19B10020-E8F2-537E-4F6C-D104768A1214"); // create service


// create switch characteristic and allow remote device to read and write
BLECharCharacteristic ledCharacteristic("19B10011-E8F2-537E-4F6C-D104768A1214", BLERead | BLEWrite);
BLECharCharacteristic actionCharacteristic("19B10012-E8F2-537E-4F6C-D104768A1214", BLERead | BLEWrite); 
BLECharCharacteristic directionCharacteristic("19B10013-E8F2-537E-4F6C-D104768A1214", BLERead | BLEWrite);
BLEIntCharacteristic lifeCharacteristic("19B10014-E8F2-537E-4F6C-D104768A1214", BLERead | BLENotify);// allows remote device to get notifications


void setup() {
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT); // use the LED on pin 13 as an output
  pinMode(actionPin, OUTPUT); // use button pin 4 as an input

  AFMS.begin();
  myMotorA->setSpeed(150);
  myMotorB->setSpeed(150);
  myMotorA->run(FORWARD);
  myMotorB->run(FORWARD);
  myMotorA->run(RELEASE);
  myMotorB->run(RELEASE);
  // set the local name peripheral advertises
  blePeripheral.setLocalName("TeamA");
  // set the UUID for the service this peripheral advertises:
  blePeripheral.setAdvertisedServiceUuid(ledService.uuid());

  // add service and characteristics
  blePeripheral.addAttribute(ledService);
  blePeripheral.addAttribute(ledCharacteristic);
  blePeripheral.addAttribute(actionCharacteristic);
  blePeripheral.setAdvertisedServiceUuid(dirService.uuid());
  blePeripheral.addAttribute(directionCharacteristic);
  blePeripheral.addAttribute(lifeCharacteristic);

  ledCharacteristic.setValue(0);
  actionCharacteristic.setValue(0);
  lifeCharacteristic.setValue(healthPoints);
  directionCharacteristic.setValue(0);

  // advertise the service
  blePeripheral.begin();

  Serial.println("Bluetooth device active, waiting for connections...");
}

void loop() {
  // poll peripheral
  blePeripheral.poll();

  if(healthPoints < prevHealthPoints)
  {
    lifeCharacteristic.setValue(healthPoints);
    prevHealthPoints = healthPoints;
  }
  

  // read the current button pin state
  
  if(actionCharacteristic.written())
  {
    Serial.print("Value : ");
    char action = actionCharacteristic.value();
    Serial.println(action);
  }

  if (ledCharacteristic.written() ) {
    // update LED, either central has written to characteristic or button state has changed
    Serial.print("Value: ");
    char ledSwitch = ledCharacteristic.value();
    Serial.print(ledSwitch);
    if (ledSwitch=='1') {
      Serial.println("LED on");
      digitalWrite(ledPin, HIGH);
    } else if(ledSwitch=='0'){
      Serial.println("LED off");
      digitalWrite(ledPin, LOW);
    }
    else
    {
      Serial.println("UNKNOWN LED VALUE");
    }
    
  }
  if(directionCharacteristic.written())
  {
    Serial.print("Direction value: ");
    char dir = directionCharacteristic.value();
    Serial.println(dir);
    switch (dir)
    {
      case '0':
      // stop
        myMotorA->run(RELEASE);
        myMotorB->run(RELEASE);
        
      break;
      case '1':
      // forward
        myMotorA->run(FORWARD);
        myMotorB->run(FORWARD);
      break;
      case '2':
      // reverse
        myMotorA->run(BACKWARD);
        myMotorB->run(BACKWARD);
      break;
      case '3':
      // left
        myMotorA->run(FORWARD);
        myMotorB->run(RELEASE);
      break;
      case '4':
      // right
        myMotorA->run(RELEASE);
        myMotorB->run(FORWARD);
      break;
      default:
      // STOP
        myMotorA->run(RELEASE);
        myMotorB->run(RELEASE);
      break;
    
    }
    delay(10);
  }
  
}

/*
  Copyright (c) 2016 Intel Corporation. All rights reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-
  1301 USA
*/
